use bufstream::BufStream;
pub mod tic_tac_toe;
pub mod battleship;

pub enum GameReturn {
    GameOver,
    PlayerDisconnected,
}

pub trait GameBoard {
  fn get_num_players(&self) -> usize;
  fn run(&self, mut players: &mut Vec<BufStream<std::net::TcpStream>>) -> GameReturn;
}
