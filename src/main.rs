use std::io;

use std::net::{TcpListener, TcpStream};
use bufstream::BufStream;
use std::io::Write;
use std::io::BufRead;
mod games;
use games::*;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;

extern crate strum;
#[macro_use]
extern crate strum_macros;

use strum::IntoEnumIterator;

#[derive(Display, EnumIter, FromPrimitive)]
enum game_selection {
    NEO_TIC_TAC_TOE,
    TIC_TAC_TOE,
    BATTLESHIP,
    MONOPOLY
}

fn get_player_one(listen_socket: &TcpListener)
-> Result<(game_selection, TcpStream), std::io::Error> {
    // Let player_1 connect
    let (mut _socket, addr) = listen_socket.accept()?;

    println!("Player #1 has connected from {}", addr);

    // TODO: Ask player1 what game they want to play tic-tac-toe
    _socket.write(b"Select which game to play:\n");
    for (i,j) in game_selection::iter().enumerate() {
        writeln!(_socket, "{} : {}", i, j);
    }
     let mut player1 = BufStream::new(&_socket);

     loop {
         let mut selection = String::new();
         player1.read_line(&mut selection).expect("Failed to read from socket");
         let selection: usize = match selection.trim().parse() {
             Ok(t) => t,
             Err(e) => {println!("{}", e); continue}
         };
         let selection = match game_selection::from_usize(selection)
         {
             Some(t) => t,
             None => {println!("invalid value"); continue}
         };
         std::mem::drop(player1);
         return Ok((selection, _socket));
     }




}

fn main()
-> io::Result<()> {
  // Hold all of our players connections.
  let mut players = Vec::new();
  let mut game_board; // Instantiate an unknown gameboard for use.

  // Set up the socket listener
  let listener = TcpListener::bind("0.0.0.0:4001")?;
  println!("Started server on port 4001");

  loop {
      match get_player_one(&listener) {
          Ok((game_board_selection, mut player1)) => {
              // This will be the object holder for our game board and is not mutable
              game_board = game_board_selection;
              player1.write(b"welcome player 1, waiting for more connections");
              players.push(BufStream::new(player1));
              break;
          }, Err(e) => {
              println!("There was an error connecting with player1: {}, are you connected to the internet?", e);
          }
      }
  }


  // Instantiate game_board
  let game_board: Box<dyn GameBoard> = match game_board {
  game_selection::TIC_TAC_TOE => Box::new(tic_tac_toe::tic_tac_toe_gameboard {}),
  game_selection::BATTLESHIP => Box::new(battleship::battleship_gameboard {}),
  _ => unimplemented!("other games not implemented")
  };

  // Wait for remaining players to connect
  for i in 1..game_board.get_num_players() {
      let (mut socket, addr) = listener.accept()?;
      writeln!(socket, "welcome player #{}", i+1);
      players.push(BufStream::new(socket));
      println!("Player #{} has connected from {}", i+1, addr);
  }

  // Run the game
  game_board.run(&mut players);

  println!("End of program reached.");
  Ok(())
}
